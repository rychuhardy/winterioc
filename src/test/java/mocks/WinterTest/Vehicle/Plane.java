package mocks.WinterTest.Vehicle;

import mocks.WinterTest.Vehicle.LandingGear.LandingGear;
import mocks.WinterTest.Vehicle.PlaneWing.PlaneWing;

/**
 * Created by Ryszard on 04/12/2016.
 */
public class Plane implements Vehicle {

    public LandingGear landingGear;
    private PlaneWing leftWing;
    public PlaneWing rightWing;

    public Plane(){}

    public Plane(LandingGear landingGear) {
        this.landingGear = landingGear;
    }

    public Plane(PlaneWing leftWing, PlaneWing rightWing) {
        this.leftWing = leftWing;
        this.rightWing = rightWing;
    }

    public LandingGear getLandingGear() {
        return landingGear;
    }

    public void setLandingGear(LandingGear landingGear) {
        this.landingGear = landingGear;
    }

    public PlaneWing getLeftWing() {
        return leftWing;
    }

    public PlaneWing getRightWing() {
        return rightWing;
    }

    public void setLeftWing(PlaneWing planeWing){
        this.leftWing = planeWing;
    }
}
