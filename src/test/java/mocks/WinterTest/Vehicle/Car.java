package mocks.WinterTest.Vehicle;

import mocks.WinterTest.Vehicle.Drive.FourWheelDrive;
import mocks.WinterTest.Vehicle.Drive.Quattro;
import mocks.WinterTest.Vehicle.EngineType.EngineType;
import mocks.WinterTest.Vehicle.EngineType.PetrolEngine;

/**
 * Created by Ryszard on 04/12/2016.
 */
public class Car implements Vehicle {
    private EngineType engineType;
    private FourWheelDrive fourWheelDrive;

    public Car(){}

    public Car(EngineType engineType, FourWheelDrive fourWheelDrive) {
        this.engineType = engineType;
        this.fourWheelDrive = fourWheelDrive;
    }

    public EngineType getEngineType() {
        return engineType;
    }

    private void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    public FourWheelDrive getFourWheelDrive() {
        return fourWheelDrive;
    }

    public void setFourWheelDrive(Quattro fourWheelDrive) {
        this.fourWheelDrive = fourWheelDrive;
    }

}
