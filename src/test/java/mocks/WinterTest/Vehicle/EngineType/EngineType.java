package mocks.WinterTest.Vehicle.EngineType;

import mocks.WinterTest.Vehicle.EngineType.Piston.Piston;

/**
 * Created by Ryszard on 04/12/2016.
 */
public interface EngineType {

    public Piston getPiston();
}
