package mocks.WinterTest.Vehicle.EngineType.Piston;

import mocks.WinterTest.Vehicle.EngineType.Piston.PistonRing.PistonRing;

import java.util.List;

/**
 * Created by Ryszard on 18/12/2016.
 */
public class SlipperPiston implements Piston {

    private List<PistonRing> pistonRings;

    public SlipperPiston(List<PistonRing> pistonRings) {
        this.pistonRings = pistonRings;
    }

    @Override
    public List<PistonRing> getPistonRings() {
        return null;
    }

}
