package mocks.WinterTest.Vehicle.EngineType.Piston;

import mocks.WinterTest.Vehicle.EngineType.Piston.PistonRing.PistonRing;

import java.util.List;

/**
 * Created by Ryszard on 18/12/2016.
 */
public interface Piston {

    public List<PistonRing> getPistonRings();
}
