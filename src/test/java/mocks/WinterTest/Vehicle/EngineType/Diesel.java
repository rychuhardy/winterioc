package mocks.WinterTest.Vehicle.EngineType;

import mocks.WinterTest.Vehicle.EngineType.Piston.Piston;

/**
 * Created by Ryszard on 04/12/2016.
 */
public class Diesel implements EngineType {

    private Piston piston;

    public Diesel() {
        this(null);
    }

    public Diesel(Piston piston) {
        this.piston = piston;
    }

    @Override
    public Piston getPiston() {
        return piston;
    }

}
