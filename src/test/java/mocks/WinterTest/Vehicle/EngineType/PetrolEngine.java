package mocks.WinterTest.Vehicle.EngineType;

import mocks.WinterTest.Vehicle.EngineType.Piston.Piston;

/**
 * Created by damian on 16.12.16.
 */
public class PetrolEngine implements EngineType {

    private Piston piston;

    public PetrolEngine() {
        this(null);
    }
    public PetrolEngine(Piston piston) {
        this.piston = piston;
    }



    @Override
    public Piston getPiston() {
        return piston;
    }

}
