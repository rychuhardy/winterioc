package tests.injector;

import configuration.ConfigurationReader;
import configuration.FileConfigurationReader;
import configuration.WinterConfiguration;
import configuration.exception.NonExistedMethodException;
import injector.Injector;
import mocks.WinterTest.Vehicle.LandingGear.SolidLandingGear;
import mocks.WinterTest.Vehicle.Plane;
import mocks.WinterTest.Vehicle.Vehicle;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by damian on 14.01.17.
 */
public class SetterInjectionTest {
    @Test
    public void testShouldInjectWithSetterMethod(){
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/SetterInjectionTest/ShouldInjectWithSetterMethod.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(
                configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

        Assert.assertTrue(vehicle instanceof Plane);
        Plane plane = (Plane) vehicle;
        Assert.assertTrue(plane.getLandingGear() instanceof SolidLandingGear);

    }

    @Test(expected = NonExistedMethodException.class)
    public void shouldThrowAfterAccessingPrivateMethodTest(){
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/SetterInjectionTest/ShouldThrowAfterAccessingPrivateMethodTest.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

    }

}
