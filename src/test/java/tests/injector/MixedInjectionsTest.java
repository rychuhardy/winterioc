package tests.injector;

import configuration.ConfigurationReader;
import configuration.FileConfigurationReader;
import configuration.WinterConfiguration;
import injector.Injector;
import mocks.WinterTest.Vehicle.LandingGear.SolidLandingGear;
import mocks.WinterTest.Vehicle.Plane;
import mocks.WinterTest.Vehicle.PlaneWing.ExquisiteTitanWing;
import mocks.WinterTest.Vehicle.PlaneWing.SolidMetalWing;
import mocks.WinterTest.Vehicle.Vehicle;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by damian on 19.01.17.
 */
public class MixedInjectionsTest {

    @Test
    public void shouldInjectWithMixedInjectionsTest() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/MixedInjectionsTest/ShouldInjectWithMixedInjections.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

        Assert.assertTrue(vehicle instanceof Plane);
        Plane plane = (Plane) vehicle;
        Assert.assertTrue(plane.getLandingGear() instanceof SolidLandingGear);
        Assert.assertTrue(plane.getLeftWing() instanceof ExquisiteTitanWing);
        Assert.assertTrue(plane.getRightWing() instanceof SolidMetalWing);
    }

    @Test
    public void shouldInjectWithDefaultConstructorAndAddedSetterAndFieldsTest() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/MixedInjectionsTest/" +
                "ShouldInjectWithDefaultConstructorAndAddedSetterAndFields.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

        Assert.assertTrue(vehicle instanceof Plane);
        Plane plane = (Plane) vehicle;
        Assert.assertTrue(plane.getLandingGear() instanceof SolidLandingGear);
        Assert.assertTrue(plane.getLeftWing() instanceof ExquisiteTitanWing);
        Assert.assertTrue(plane.getRightWing() instanceof SolidMetalWing);
    }


}
