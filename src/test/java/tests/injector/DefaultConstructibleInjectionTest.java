package tests.injector;

import configuration.WinterConfiguration;
import configuration.exception.ClassNotRegisteredException;
import injector.Injector;
import mocks.WinterTest.Vehicle.EngineType.Piston.CrossheadPiston;
import mocks.WinterTest.Vehicle.LandingGear.LandingGear;
import mocks.WinterTest.Vehicle.LandingGear.SolidLandingGear;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Ryszard on 18/12/2016.
 */
public class DefaultConstructibleInjectionTest {

    @Test
    public void testShouldInjectUnregisteredDefaultConstructibleClass() {
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        Injector injector = Injector.create(winterConfiguration);
        SolidLandingGear solidLandingGear = null;

        solidLandingGear = (SolidLandingGear) injector.getInstance(SolidLandingGear.class);

        Assert.assertTrue(solidLandingGear instanceof SolidLandingGear);
    }

    @Test(expected = ClassNotRegisteredException.class)
    public void testShouldNotInjectUnregisteredInterface() {
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        Injector injector = Injector.create(winterConfiguration);
        LandingGear landingGear = null;

        landingGear = (LandingGear) injector.getInstance(LandingGear.class);

        Assert.assertNull(landingGear);
    }

    @Test(expected = ClassNotRegisteredException.class)
    public void testShouldNotInjectClassWithNoDefaultConstructor() {
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        Injector injector = Injector.create(winterConfiguration);
        CrossheadPiston crossheadPiston = null;

        crossheadPiston = (CrossheadPiston) injector.getInstance(CrossheadPiston.class);

        Assert.assertNull(crossheadPiston);
    }

}
