package tests.injector;

import configuration.ConfigurationReader;
import configuration.FileConfigurationReader;
import configuration.WinterConfiguration;
import injector.Injector;
import injector.exception.ConstructorMatchException;
import mocks.WinterTest.Vehicle.Car;
import mocks.WinterTest.Vehicle.Vehicle;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Ryszard on 18/12/2016.
 */
public class ConstructorSelectionTest {

    @Test
    public void testShouldCreateInstanceWhenConstructorIsNotSpecifiedInConfigurationWithDefaultConstructor() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/ConstructorSelectionTest/ShouldCreateInstanceWhenConstructorInNotSpecifiedInConfigurationWithDefaultConstructor.xml");
        WinterConfiguration configuration = new WinterConfiguration();
        configuration.read(configurationReader);
        Injector injector = Injector.create(configuration);

        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);
        Assert.assertTrue(vehicle instanceof Car);
    }

    @Test(expected = ConstructorMatchException.class)
    public void testShouldNotCreateInstanceWhenConstructorParameterTypesDiffer() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/ConstructorSelectionTest/ShouldNotCreateInstanceWhenConstructorParameterTypesDiffer.xml");
        WinterConfiguration configuration = new WinterConfiguration();
        configuration.read(configurationReader);
        Injector injector = Injector.create(configuration);

        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);
        Assert.assertNull(vehicle);
    }

    @Test(expected = ConstructorMatchException.class)
    public void testShouldNotCreateInstanceWhenConstructorParametersCountDiffer() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/ConstructorSelectionTest/ShouldNotCreateInstanceWhenConstructorParametersCountDiffer.xml");
        WinterConfiguration configuration = new WinterConfiguration();
        configuration.read(configurationReader);
        Injector injector = Injector.create(configuration);

        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);
        Assert.assertNull(vehicle);
    }

    @Test(expected = ConstructorMatchException.class)
    public void testShouldThrowWhenMoreThanOneConstructorInSpecifiedInConfiguration() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/ConstructorSelectionTest/ShouldThrowWhenMoreThanOneConstructorInSpecifiedInConfiguration.xml");
        WinterConfiguration configuration = new WinterConfiguration();
        configuration.read(configurationReader);
        Injector injector = Injector.create(configuration);

        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);
        Assert.assertNull(vehicle);
    }

    @Test(expected = ConstructorMatchException.class)
    public void testShouldThrowWhenSpecifiedConstructorIsNotPresent() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/ConstructorSelectionTest/ShouldNotCreateInstanceWhenConstructorParameterTypesDiffer.xml");
        WinterConfiguration configuration = new WinterConfiguration();
        configuration.read(configurationReader);
        Injector injector = Injector.create(configuration);

        Car vehicle = null;

        vehicle = (Car) injector.getInstance(Vehicle.class);
        Assert.assertNull(vehicle);
    }

}
