package tests.injector;

import configuration.ConfigurationReader;
import configuration.FileConfigurationReader;
import configuration.WinterConfiguration;
import injector.Injector;
import mocks.WinterTest.Vehicle.Car;
import mocks.WinterTest.Vehicle.Drive.Quattro;
import mocks.WinterTest.Vehicle.EngineType.Diesel;
import mocks.WinterTest.Vehicle.LandingGear.SolidLandingGear;
import mocks.WinterTest.Vehicle.Plane;
import mocks.WinterTest.Vehicle.Vehicle;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by damian on 04.12.16.
 */
public class InjectUnnamedInterfaceTest {

    @Test
    public void testShouldInjectPlaneAndSolidLandingGear() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/InjectUnnamedInterfaceTest/ShouldInjectPlaneAndSolidLandingGear.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);
        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

        Assert.assertTrue(vehicle instanceof Plane);
        Plane plane = (Plane) vehicle;
        Assert.assertTrue(plane.getLandingGear() instanceof SolidLandingGear);
    }


    @Test
    public void testShouldInjectCarWithDieselAndQuattro() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/InjectUnnamedInterfaceTest/ShouldInjectCarWithDieselAndQuattro.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);
        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

        Assert.assertTrue(vehicle instanceof Car);
        Car car = (Car) vehicle;
        Assert.assertTrue(car.getEngineType() instanceof Diesel);
        Assert.assertTrue(car.getFourWheelDrive() instanceof Quattro);
    }

}
