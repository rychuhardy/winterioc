package tests.injector;

import configuration.ConfigurationReader;
import configuration.FileConfigurationReader;
import configuration.WinterConfiguration;
import configuration.exception.ClassNotRegisteredException;
import injector.Injector;
import mocks.WinterTest.Vehicle.Car;
import mocks.WinterTest.Vehicle.Drive.Quattro;
import mocks.WinterTest.Vehicle.EngineType.PetrolEngine;
import mocks.WinterTest.Vehicle.Plane;
import mocks.WinterTest.Vehicle.PlaneWing.ExquisiteTitanWing;
import mocks.WinterTest.Vehicle.PlaneWing.SolidMetalWing;
import mocks.WinterTest.Vehicle.Vehicle;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Ryszard on 19/12/2016.
 */
public class NamedDependenciesTest {

    @Test
    public void testShouldInjectPetrolEngineAndQuattroNamedPetrolInjection() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/NamedDependenciesTest/ShouldInjectPetrolEngineAndQuattro.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

        Assert.assertTrue(vehicle instanceof Car);
        Car car = (Car) vehicle;
        Assert.assertTrue(car.getEngineType() instanceof PetrolEngine);
        Assert.assertTrue(car.getFourWheelDrive() instanceof Quattro);
    }

    @Test(expected = ClassNotRegisteredException.class)
    public void testShouldThrowWhenTryingToCreateFromNonExistingNamedDependency() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/NamedDependenciesTest/ShouldThrowWhenTryingToCreateFromNonExistingNamedDependency.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);
    }

    @Test
    public void testShouldInjectTwoDifferentImplementationsOfPlaneWingToPlane() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/NamedDependenciesTest/ShouldInjectTwoDifferentImplementationsOfPlaneWingToPlane.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(
                configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

        Assert.assertTrue(vehicle instanceof Plane);
        Plane plane = (Plane) vehicle;
        Assert.assertTrue(plane.getLeftWing() instanceof SolidMetalWing);
        Assert.assertTrue(plane.getRightWing() instanceof ExquisiteTitanWing);
    }

}
