package tests.injector;

import configuration.ConfigurationReader;
import configuration.FileConfigurationReader;
import configuration.WinterConfiguration;
import injector.Injector;
import mocks.WinterTest.Vehicle.Vehicle;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by damian on 11.01.17.
 */
public class SingletonTest {

    @Test
    public void shouldReturnTheSameInstance() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/SingletonTest/ReturnTheSameInstance.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle = null;
        Vehicle vehicle2 = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

        vehicle2 = (Vehicle) injector.getInstance(Vehicle.class);


        Assert.assertTrue(vehicle == vehicle2);
    }

    @Test
    public void shouldReturnDifferentInstances() {
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/SingletonTest/ShouldReturnDifferentInstances.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle = null;
        Vehicle vehicle2 = null;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

        vehicle2 = (Vehicle) injector.getInstance(Vehicle.class);


        Assert.assertFalse(vehicle == vehicle2);
    }
}
