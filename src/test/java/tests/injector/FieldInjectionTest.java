package tests.injector;

import configuration.ConfigurationReader;
import configuration.FileConfigurationReader;
import configuration.WinterConfiguration;
import configuration.exception.AccessToPrivateFieldException;
import configuration.exception.ClassNotRegisteredException;
import configuration.exception.NonExistedFieldException;
import injector.Injector;
import mocks.WinterTest.Vehicle.LandingGear.SolidLandingGear;
import mocks.WinterTest.Vehicle.Plane;
import mocks.WinterTest.Vehicle.Vehicle;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by damian on 19.01.17.
 */
public class FieldInjectionTest {

    @Test
    public void shouldInjectWithFieldTest(){
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/FieldInjectionTest/ShouldInjectWithField.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

        Assert.assertTrue(vehicle instanceof Plane);
        Plane plane = (Plane) vehicle;
        Assert.assertTrue(plane.getLandingGear() instanceof SolidLandingGear);
    }

    @Test(expected = ClassNotRegisteredException.class)
    public void shouldNotInjectWithUnregisteredFieldTest(){
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/FieldInjectionTest/ShouldNotInjectWithUnregisteredField.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

    }

    @Test(expected = NonExistedFieldException.class)
    public void shouldNotInjectWithNoExistedFieldTest(){
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/FieldInjectionTest/ShouldNotInjectWithNoExistedField.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);

    }
    @Test(expected = AccessToPrivateFieldException.class)
    public void shouldThrowAfterAccessingToPrivateFieldTest(){
        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/FieldInjectionTest/ShouldThrowAfterAccessingToPrivateField.xml");
        WinterConfiguration winterConfiguration = new WinterConfiguration();
        winterConfiguration.read(configurationReader);
        Injector injector = Injector.create(winterConfiguration);

        Vehicle vehicle;

        vehicle = (Vehicle) injector.getInstance(Vehicle.class);
    }
}
