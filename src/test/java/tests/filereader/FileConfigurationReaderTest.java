package tests.filereader;

import configuration.ConfigurationReader;
import configuration.FileConfigurationReader;
import configuration.WinterConfiguration;
import configuration.exception.ConfigurationFileException;
import injector.Injector;
import org.junit.Test;

/**
 * Created by Ryszard on 19/01/2017.
 */
public class FileConfigurationReaderTest {

    @Test(expected = ConfigurationFileException.class)
    public void testShouldThrowWhenFileFormatIsInvalid() {

        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/filereader/ShouldThrowWhenFileFormatIsInvalid.xml");
        WinterConfiguration configuration = new WinterConfiguration();
        configuration.read(configurationReader);

    }

    @Test(expected = ConfigurationFileException.class)
    public void testShouldThrowWhenMissingClassAttributeInRegisterTag() {

        ConfigurationReader configurationReader = new FileConfigurationReader("src/test/configuration/filereader/ShouldThrowWhenFileFormatIsInvalid.xml");
        WinterConfiguration configuration = new WinterConfiguration();
        configuration.read(configurationReader);

    }
}
