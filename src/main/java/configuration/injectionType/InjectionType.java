package configuration.injectionType;

/**
 * Created by damian on 12.01.17.
 */
public enum InjectionType {
    constructor,
    setter,
    field
}
