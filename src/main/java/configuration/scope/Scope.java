package configuration.scope;

/**
 * Created by rsikora on 12/19/16.
 */
public enum Scope {
    singleton,
    prototype

}