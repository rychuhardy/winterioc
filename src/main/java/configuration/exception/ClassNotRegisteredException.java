package configuration.exception;

/**
 * Created by Ryszard on 18/12/2016.
 */
public class ClassNotRegisteredException extends RuntimeException {

    private String message;

    public ClassNotRegisteredException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    void append(String message) {
        this.message += "\n" + message;
    }
}
