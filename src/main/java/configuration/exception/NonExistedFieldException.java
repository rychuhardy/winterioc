package configuration.exception;

/**
 * Created by damian on 20.01.17.
 */
public class NonExistedFieldException extends RuntimeException {
    private String message;

    public NonExistedFieldException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    void append(String message) {
        this.message += "\n" + message;
    }
}
