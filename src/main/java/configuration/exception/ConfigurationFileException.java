package configuration.exception;

/**
 * Created by Ryszard on 18/12/2016.
 */
public class ConfigurationFileException extends RuntimeException {

    private String message;

    public ConfigurationFileException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
