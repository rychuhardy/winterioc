package configuration.exception;

/**
 * Created by damian on 20.01.17.
 */
public class AccessToPrivateFieldException extends RuntimeException {
    private String message;

    public AccessToPrivateFieldException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
