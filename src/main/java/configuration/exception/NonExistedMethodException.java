package configuration.exception;

/**
 * Created by damian on 20.01.17.
 */
public class NonExistedMethodException extends RuntimeException {
    private String message;

    public NonExistedMethodException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
