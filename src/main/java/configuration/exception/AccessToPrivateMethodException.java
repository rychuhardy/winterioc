package configuration.exception;

/**
 * Created by damian on 20.01.17.
 */
public class AccessToPrivateMethodException extends RuntimeException {
    private String message;

    public AccessToPrivateMethodException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
