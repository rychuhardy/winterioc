package configuration;

import configuration.exception.ConfigurationFileException;
import configuration.injectionType.InjectionType;
import configuration.scope.Scope;
import configuration.type.ConfiguredType;
import configuration.type.ImplementedType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Map;

/**
 * Created by Ryszard on 04/12/2016.
 */
public class RegisterTagReader {

    private static final String CLASS_ATTR = "class";
    private static final String SCOPE_ATTR = "scope";
    private static final String IMPLEMENTATION_ATTR = "implementation";
    private static final String NAME_ATTR = "name";
    private static final String CTOR_TAG = "ctor";
    private static final String PROP_TAG = "prop";

    private static final String SCOPE_ATTR_VALUE_PROTOTYPE = "prototype";
    private static final String FIELD_TAG = "field";

    private NodeList nodeList;
    private String configurationFilePath;

    public RegisterTagReader(NodeList nList, String configurationFilePath) {
        nodeList = nList;
        this.configurationFilePath = configurationFilePath;
    }

    public void read(Map<ConfiguredType, ImplementedType> configuredMapping) {

        for (int idx = 0; idx < nodeList.getLength(); idx++) {
            Node node = nodeList.item(idx);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                Class clazz = getRequiredAttr(element, CLASS_ATTR);
                Scope scope = getScopeOrDefault(element);
                Class implementation = getImplementationOrDefault(element);
                String name = element.getAttribute(NAME_ATTR);
                ConfiguredType configuredType = new ConfiguredType(clazz, name);
                ImplementedType implementedType = new ImplementedType(implementation, scope);

                    if (element.getElementsByTagName(PROP_TAG).getLength() != 0) {
                        //if (element.getElementsByTagName(CTOR_TAG).getLength() != 0)
                        NodeList propNodeList = element.getElementsByTagName(PROP_TAG);
                        ElementsReader.readElements(implementedType, propNodeList, InjectionType.setter, configurationFilePath, "register/prop");
                    }
                    if (element.getElementsByTagName(CTOR_TAG).getLength() != 0){
                        CtorTagReader ctorTagReader = new CtorTagReader(element.getElementsByTagName(CTOR_TAG), configurationFilePath);
                        ctorTagReader.read(implementedType);
                    }
                    if (element.getElementsByTagName(FIELD_TAG).getLength() != 0){
                        NodeList fieldNodeList = element.getElementsByTagName(FIELD_TAG);
                        ElementsReader.readElements(implementedType,fieldNodeList,InjectionType.field,  configurationFilePath, "register/field");
                    }

                configuredMapping.put(configuredType, implementedType);
            }
        }
    }

    private Class getRequiredAttr(Element element, String classAttr) {
        String value = element.getAttribute(classAttr);
        if (value.isEmpty()) {
            throw new ConfigurationFileException("Expected value for required attribute " + classAttr);
        }
        try {
            Class c = Class.forName(value);
            return c;
        } catch (ClassNotFoundException e) {
            throw new ConfigurationFileException("Class: " + value + " specified in the register/class field of the configuration file: " + configurationFilePath + " not found");
        }

    }

    private Class getImplementationOrDefault(Element element) {
        String implementationAttr = element.getAttribute(IMPLEMENTATION_ATTR);
        if (implementationAttr.isEmpty()) {
            return getRequiredAttr(element, CLASS_ATTR);
        }
        try {
            return Class.forName(implementationAttr);
        } catch (ClassNotFoundException e) {
            throw new ConfigurationFileException("Class: " + implementationAttr + " specified in the register/implementation field of the configuration file: " + configurationFilePath + " not found");
        }
    }

    private Scope getScopeOrDefault(Element element) {
        String scopeVal = element.getAttribute(SCOPE_ATTR);
        if (scopeVal.isEmpty()) {
            return Scope.valueOf(SCOPE_ATTR_VALUE_PROTOTYPE);
        }
        try {
            Scope scope = Scope.valueOf(scopeVal);
            return scope;
        } catch (IllegalArgumentException exception) {
            throw new ConfigurationFileException("register/scope attribute must be one of {prototype, singleton} in the configuration file: " + configurationFilePath);
        }
    }
}
