package configuration;

import configuration.exception.ConfigurationFileException;
import configuration.injectionType.InjectionType;
import configuration.tags.Tags;
import configuration.type.ImplementedType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by damian on 16.12.16.
 */
public class CtorTagReader {
    private NodeList nodeList;
    private String configurationFilePath;
    private final static String attributePath = "register/ctor/param";

    public CtorTagReader(NodeList nodeList, String configurationFilePath) {
        this.configurationFilePath = configurationFilePath;

        if (nodeList.getLength() > 1) {
            throw new ConfigurationFileException("More than one constructor register/ctor specified in the configuration file: " + configurationFilePath);
        } else if (nodeList.getLength() == 1) {
            Node node = nodeList.item(0);
            Element element = (Element) node;
            this.nodeList = element.getElementsByTagName(Tags.PARAM_TAG);
        }
    }

    public void read(ImplementedType implementedType) {

        if (nodeList == null || nodeList.getLength() == 0) {
            return;
        }
        ElementsReader.readElements(implementedType, nodeList, InjectionType.constructor, configurationFilePath, attributePath);
    }

}
