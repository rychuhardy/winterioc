package configuration;

import configuration.exception.ClassNotRegisteredException;
import configuration.type.ConfiguredType;
import configuration.type.ImplementedType;

import java.util.*;

/**
 * Created by Ryszard on 27/11/2016.
 */
public class WinterConfiguration {

    private Map<ConfiguredType, ImplementedType> configuredMapping;

    public WinterConfiguration() {
        configuredMapping = new HashMap<>();
    }

    public void read(ConfigurationReader configurationReader) {
        configuredMapping = configurationReader.read();
    }

    public ImplementedType getImplementationOf(ConfiguredType configuredType) {

        ImplementedType result = configuredMapping.get(configuredType);

        if (result == null) {
            throw new ClassNotRegisteredException("Could not create instance of " + configuredType.getClassType().toString()
                    + (configuredType.getName() == null ? "" : " annotated with name " + configuredType.getName()));
        }

        return result;
    }

}
