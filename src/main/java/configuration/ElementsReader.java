package configuration;

import configuration.exception.ConfigurationFileException;
import configuration.injectionType.InjectionType;
import configuration.tags.Tags;
import configuration.type.ImplementedType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Created by damian on 14.01.17.
 */
public class ElementsReader {
    public static void readElements(ImplementedType implementedType, NodeList nodeList, InjectionType injectionType, String configurationFilePath, String attributePath){
        for (int idx = 0; idx < nodeList.getLength(); idx++) {
            Node node = nodeList.item(idx);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String classAttr = element.getAttribute(Tags.CLASS_ATTR);
                String name = element.getAttribute(Tags.NAME_ATTR);
                String id = element.getAttribute(Tags.ID_ATTR);
                Parameter param = null;
                try {
                    param = new Parameter(Class.forName(classAttr), name, id);

                } catch (ClassNotFoundException e) {
                    throw new ConfigurationFileException("Class: " + classAttr + " specified in the " + attributePath + "/" + Tags.CLASS_ATTR + " field of the configuration file: " + configurationFilePath + " not found");
                }
                switch (injectionType){
                    case constructor:
                        implementedType.addCtorParam(param);
                        break;
                    case field:
                        implementedType.addFieldParam(param);
                        break;
                    case setter:
                        implementedType.addSetterParam(param);
                        break;
                }
            }
        }
    }
}
