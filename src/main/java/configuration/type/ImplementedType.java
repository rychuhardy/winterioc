package configuration.type;

import configuration.Parameter;
import configuration.injectionType.InjectionType;
import configuration.scope.Scope;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by damian on 16.12.16.
 */
public class ImplementedType {
    private Class c;
    private List<Parameter> ctorParams;
    private List<Parameter> setterParams;
    private List<Parameter> fieldParams;
    private Scope scope;

    public ImplementedType(Class c){
        this(c, Scope.prototype);
    }

    public ImplementedType(Class c, Scope scope){
        this.c = c;
        this.scope = scope;
        this.ctorParams = new ArrayList<>();
        this.setterParams = new ArrayList<>();
        this.fieldParams = new ArrayList<>();
    }

    public ImplementedType(Class c, Scope scope, InjectionType injectionType){
        this.c = c;
        this.scope = scope;
        this.ctorParams = new ArrayList<>();
        this.setterParams = new ArrayList<>();
        this.fieldParams = new ArrayList<>();
    }

    public void addCtorParam(Parameter param){
        ctorParams.add(param);
    }

    public void addSetterParam(Parameter param) {setterParams.add(param);}

    public void addFieldParam(Parameter param) {fieldParams.add(param);}

    public Class getC() {
        return c;
    }

    public void setC(Class c) {
        this.c = c;
    }

    public List<Parameter> getCtorParams(){
        return ctorParams;
    }

    public Scope getScope() {
        return scope;
    }


    public List<Parameter> getSetterParams() {
        return setterParams;
    }

    public void setSetterParams(List<Parameter> setterParams) {
        this.setterParams = setterParams;
    }

    public List<Parameter> getFieldParams() {
        return fieldParams;
    }

    public void setFieldParams(List<Parameter> fieldParams) {
        this.fieldParams = fieldParams;
    }
}
