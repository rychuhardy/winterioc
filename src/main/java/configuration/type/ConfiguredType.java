package configuration.type;

import java.util.Objects;

/**
 * Created by Ryszard on 27/11/2016.
 */
public class ConfiguredType {
    private Class c;
    //    private Scope scope;
    private String name;

    public ConfiguredType(Class c) {
        this(c, null);
    }

    public ConfiguredType(Class c, String name) {
        this.c = c;
        this.name = name;
    }

    public Class getClassType() {
        return c;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object other) {
        ConfiguredType cType = (ConfiguredType) other;
        if (cType == null) {
            return false;
        }
        if (cType == this) {
            return true;
        }
        return c.equals(cType.c) && (name == null || name.equals(cType.name));
    }

    @Override
    public int hashCode() {
        return Objects.hash(c, name);
    }
}
