package configuration;

import configuration.exception.ConfigurationFileException;
import configuration.type.ConfiguredType;
import configuration.type.ImplementedType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static configuration.tags.Tags.REGISTER_TAG;
import static configuration.tags.Tags.ROOT_TAG;

/**
 * Created by damian on 08.01.17.
 */
public class FileConfigurationReader implements ConfigurationReader {

    private String configurationFilePath;

    public FileConfigurationReader(String configurationFilePath) {
        this.configurationFilePath = configurationFilePath;
    }

    @Override
    public Map read() {
        Map<ConfiguredType, ImplementedType> configuredMapping = new HashMap<>();
        File inputFile = new File(configurationFilePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        Document doc = null;

        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(inputFile);
        } catch (SAXException e) {
            throw new ConfigurationFileException("Invalid file format: " + e.getMessage());
        } catch (IOException e) {
            throw new ConfigurationFileException("An IO error ocurred");
        } catch (ParserConfigurationException e) {
            throw new ConfigurationFileException("Parser error ocurred");
        }

        doc.getDocumentElement().normalize();

        NodeList nodeList = doc.getElementsByTagName(ROOT_TAG);

        if (nodeList.getLength() == 0) {
            return null;
        }

        if (nodeList.getLength() > 1) {
            throw new ConfigurationFileException("There can be only one " + ROOT_TAG + " in the configuration file: " + configurationFilePath);
        }

        Element rootElement = (Element) nodeList.item(0);

        RegisterTagReader registerTagReader = new RegisterTagReader(rootElement.getElementsByTagName(REGISTER_TAG), configurationFilePath);
        registerTagReader.read(configuredMapping);

        return configuredMapping;
    }
}
