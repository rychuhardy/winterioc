package configuration.tags;

/**
 * Created by damian on 14.01.17.
 */
public class Tags {
    public static final String PARAM_TAG = "param";
    public static final String CLASS_ATTR = "class";
    public static final String NAME_ATTR = "name";
    public static final String SCOPE_ATTR = "scope";
    public static final String IMPLEMENTATION_ATTR = "implementation";
    public static final String CTOR_TAG = "ctor";
    public static final String PROP_TAG = "prop";
    public static final String ROOT_TAG = "winter";
    public static final String REGISTER_TAG = "register";
    public static final String ID_ATTR = "id";
}
