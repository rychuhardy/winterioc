package configuration;

/**
 * Created by damian on 17.12.16.
 */
public class Parameter {
    private Class c;
    private String name;
    private String id;

    public Parameter(Class c, String name, String id) {
        this.c = c;
        this.name = name;
        this.id = id;
    }

    public Class getC() {
        return c;
    }

    public void setC(Class c) {
        this.c = c;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
