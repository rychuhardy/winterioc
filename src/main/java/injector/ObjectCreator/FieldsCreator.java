package injector.ObjectCreator;

import configuration.Parameter;
import configuration.exception.AccessToPrivateFieldException;
import configuration.exception.NonExistedFieldException;
import configuration.type.ImplementedType;
import injector.Injector;

import java.lang.reflect.Field;

/**
 * Created by damian on 19.01.17.
 */
public class FieldsCreator {

    public static Object setFields(Object object, ImplementedType implementedType, Injector injector){

        for (Parameter param: implementedType.getFieldParams()){
            Field field;
            Object injectingObject;
            String fieldName = param.getId();
            if (fieldName.equals("")) {
                int lastIndex = param.getC().getName().lastIndexOf('.');
                fieldName = param.getC().getName().substring(lastIndex + 1, lastIndex + 2).toLowerCase() +
                        param.getC().getName().substring(lastIndex + 2);
            }
            if (param.getName().equals("")){
                injectingObject = injector.getInstance(param.getC());
            }
            else
                injectingObject = injector.getInstance(param.getC(),param.getName());

            try {
                field = object.getClass().getDeclaredField(fieldName);
                field.set(object,injectingObject);
            } catch (NoSuchFieldException e) {
                throw new NonExistedFieldException("Cannot find field: " + fieldName);
            } catch (IllegalAccessException e) {
                throw new AccessToPrivateFieldException("Expected field " +fieldName + " is private. Change it to public");
            }
        }


        return object;
    }
}
