package injector.ObjectCreator;

import configuration.Parameter;
import configuration.exception.AccessToPrivateMethodException;
import configuration.exception.NonExistedMethodException;
import configuration.type.ImplementedType;
import injector.Injector;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Ryszard on 15/01/2017.
 */
public class SetterCreator {

    public static Object setParameters(Object object, ImplementedType implementedType, Injector injector) {
        for (Parameter prop : implementedType.getSetterParams()) {
            Method setter;
            String setterMethodName;
            Object setterObj;
            if (prop.getId().equals("")) {
                int lastIndex = prop.getC().getName().lastIndexOf('.');
                setterMethodName = "set" + prop.getC().getName().substring(lastIndex + 1);
            }
            else
                setterMethodName = "set" + prop.getId().substring(0,1).toUpperCase() + prop.getId().substring(1);
            if (prop.getName().equals("")){
                setterObj = injector.getInstance(prop.getC());
            }
            else{
                setterObj = injector.getInstance(prop.getC(),prop.getName());
            }
            try {
                setter = object.getClass().getMethod(setterMethodName, prop.getC());
                setter.invoke(object, setterObj);
            } catch (NoSuchMethodException e) {
                throw new NonExistedMethodException("Cannot find method: " + setterMethodName + " in class " + object.getClass().toString()+
                        " Maybe this method is private?");
            } catch (IllegalAccessException e) {
                throw new AccessToPrivateMethodException("Setter method" + setterMethodName + "is private. Change it to public");
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return object;
    }
}
