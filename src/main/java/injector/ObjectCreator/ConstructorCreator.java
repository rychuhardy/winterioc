package injector.ObjectCreator;

import configuration.type.ConfiguredType;
import configuration.type.ImplementedType;
import injector.Injector;
import injector.exception.ConstructorMatchException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ryszard on 15/01/2017.
 */
public class ConstructorCreator {

    public static Object defaultConstruct(ConfiguredType configuredType) {
        Constructor[] ctors = configuredType.getClassType().getConstructors();
        return createDefaultObject(ctors, configuredType.getClassType());

    }

    public static Object defaultConstruct(ImplementedType implementedType) {
        Constructor[] ctors = implementedType.getC().getConstructors();
        return createDefaultObject(ctors, implementedType.getC());
    }

    private static Object createDefaultObject(Constructor[] ctors, Class c) {
        for (Constructor ctor : ctors) {
            if (ctor.getParameterTypes().length == 0) {
                try {
                    return ctor.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    throw new ConstructorMatchException("Expected a public constructor for class:" +
                            c.toString());
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        throw new ConstructorMatchException("Expected a constructor definition or default constructor for class:" +
                c.toString());
    }

    public static Object getInstance(Constructor constructor, ImplementedType implementedType, Injector injector) {
        Class[] params = constructor.getParameterTypes();
        List<Object> ctorArgs = new ArrayList();
        for (int j = 0; j < params.length; ++j) {
            String paramName = implementedType.getCtorParams().get(j).getName();
            Object paramObject = null;
            paramObject = injector.getInstance(params[j], paramName);
            ctorArgs.add(paramObject);

        }
        try {
            return constructor.newInstance(ctorArgs.toArray());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

}
