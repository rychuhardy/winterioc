package injector.exception;

/**
 * Created by Ryszard on 18/12/2016.
 */
public class ConstructorMatchException extends RuntimeException {

    private String message;

    public ConstructorMatchException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
