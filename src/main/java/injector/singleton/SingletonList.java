package injector.singleton;


import configuration.type.ConfiguredType;

import java.util.Map;
import java.util.Objects;

/**
 * Created by Ryszard on 11/12/2016.
 */
public class SingletonList {

    private Map<ConfiguredType, Object> singletons;

    public SingletonList(Map<ConfiguredType, Object> singletons) {
        this.singletons = singletons;
    }

    public Object getInstance(ConfiguredType ctype) {
        Object instance = singletons.get(ctype);
        return instance;
    }

    public void addInstance(ConfiguredType ctype, Object o) {
        singletons.put(ctype, o);
    }
}