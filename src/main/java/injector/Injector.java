package injector;

import configuration.WinterConfiguration;
import configuration.exception.ClassNotRegisteredException;
import configuration.scope.Scope;
import configuration.type.ConfiguredType;
import configuration.type.ImplementedType;
import injector.ObjectCreator.ConstructorCreator;
import injector.ObjectCreator.FieldsCreator;
import injector.ObjectCreator.SetterCreator;
import injector.constructorMatcher.ConstructorMatcher;
import injector.constructorMatcher.MatchOneOrDefault;
import injector.singleton.SingletonList;

import java.lang.reflect.Constructor;
import java.util.HashMap;


/**
 * Created by Ryszard on 27/11/2016.
 */
public class Injector {

    private WinterConfiguration configuration;
    private ConstructorMatcher constructorMatcher;
    private SingletonList singletonList;

    private Injector(WinterConfiguration configuration) {
        this.configuration = configuration;
        this.constructorMatcher = new MatchOneOrDefault();
        this.singletonList = new SingletonList(new HashMap<>());
    }

    public static Injector create(WinterConfiguration configuration) {
        return new Injector(configuration);
    }

    public Object getInstance(Class c) {
        return getInstanceInternal(new ConfiguredType(c));
    }

    public Object getInstance(Class c, String name) {
        return getInstanceInternal(new ConfiguredType(c, name));
    }

    private Object getInstanceInternal(ConfiguredType configuredType) {

        ImplementedType implementedType = null;
        try {
            implementedType = configuration.getImplementationOf(configuredType);
        } catch (ClassNotRegisteredException exception) {
            if (constructorMatcher.hasDefaultConstructor(configuredType)) {
                return ConstructorCreator.defaultConstruct(configuredType);
            }
            throw exception;
        }
        if (implementedType.getScope().equals(Scope.prototype)) {
            return getPrototypeInstance(configuredType, implementedType);
        } else {
            Object result = singletonList.getInstance(configuredType);
            if (result == null) {
                result = getPrototypeInstance(configuredType, implementedType);
                singletonList.addInstance(configuredType, result);
            }
            return result;
        }

    }

    private Object getPrototypeInstance(ConfiguredType configuredType, ImplementedType implementedType) {

        if (implementedType.getCtorParams().size() != 0) {
            Constructor constructor = constructorMatcher.matchConstructor(configuredType, implementedType);
            Object object =  ConstructorCreator.getInstance(constructor, implementedType, this);
            return addFieldAndSetterDependencies(object, implementedType);

        } else  {
            Object object = ConstructorCreator.defaultConstruct(implementedType);
            return addFieldAndSetterDependencies(object, implementedType);
        }
    }

    private Object addFieldAndSetterDependencies(Object object, ImplementedType implementedType){
        if (implementedType.getSetterParams().size() != 0 ){
            object = SetterCreator.setParameters(object, implementedType, this);
        }
        if (implementedType.getFieldParams().size() != 0){
            object = FieldsCreator.setFields(object, implementedType,this);
        }
        return object;
    }

}
