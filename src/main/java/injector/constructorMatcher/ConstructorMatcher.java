package injector.constructorMatcher;

import configuration.type.ConfiguredType;
import configuration.type.ImplementedType;

import java.lang.reflect.Constructor;

/**
 * Created by Ryszard on 18/12/2016.
 */
public interface ConstructorMatcher {

    boolean hasDefaultConstructor(ConfiguredType configuredType);

    Constructor matchConstructor(ConfiguredType configuredType, ImplementedType implementedType);
}
