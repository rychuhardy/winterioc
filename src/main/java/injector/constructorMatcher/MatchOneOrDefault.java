package injector.constructorMatcher;

import configuration.type.ConfiguredType;
import configuration.type.ImplementedType;
import injector.exception.ConstructorMatchException;

import java.lang.reflect.Constructor;
import java.util.stream.Stream;

/**
 * Created by Ryszard on 18/12/2016.
 */
public class MatchOneOrDefault implements ConstructorMatcher {

    @Override
    public boolean hasDefaultConstructor(ConfiguredType configuredType) {
        return Stream.of(configuredType.getClassType().getConstructors())
                .anyMatch((c) -> c.getParameterCount() == 0);
    }

    @Override
    public Constructor matchConstructor(ConfiguredType configuredType, ImplementedType implementedType) {

        Constructor[] ctors = implementedType.getC().getConstructors();
        for (int idx = 0; idx < ctors.length; ++idx) {
            Constructor ctor = ctors[idx];
            Class[] params = ctor.getParameterTypes();
            if (params.length == implementedType.getCtorParams().size()) {
                boolean allMatch = true;
                for (int j = 0; j < params.length && allMatch; ++j) {
                    if (!params[j].equals(implementedType.getCtorParams().get(j).getC())) {
                        allMatch = false;
                        break;
                    }
                }
                if (allMatch) {
                    return ctor;
                }
            }
        }
        throw new ConstructorMatchException("Declared constructor not found for class:" +
                configuredType.getClassType().toString());
    }

}
